05-05-2023

Just finished my 5-1 vacation. Or holiday? I don't know if there's a proper translation, just let it be.

I was planning to go to some foreign countries to play in some real shooting ranges months before, but that didn't come true, part because of all the post-covid drama that led to the visa restriction for visitors from China mainland, and part because of my easy-shakened determenation deep inside me. Maybe I wasn't longing for that anyway. And I think it doesn't matter anymore, just let it go with the wind.

Oh how about things from work? Intense project schedules, diffcult tasks, unfamiliar domains, but I can manage to get home on time, mostly.  So I have not much to blame, it's what I've been paid for after all. But my contract is going to end in the end of July, which is bothering me somehow. If I have to find another job for the routine life and the stable income, the spare time that allowed me to prepare for RUN will be pretty much limited, but if I give up on finding a new job, the fear of running out of rescoures will overwhelm me so hard that I can not even breath normally. So my best hope is that the current company will renew another 4-year contrat with me ,to buy me just a little more time to get my things done. But based on my performance, I'd say that's not something I can granted for. Nobody can.

Of couse I know there's a huge gap since last dairy entry has been written, and I'm not going to explain it, let my action explain.



Grammaly revison

> Just finished my 5-1 vacation. Or holiday? I don't know if there's a proper translation, just let it be.
> I was planning to go to some foreign countries to play in some real shooting ranges months before, but that didn't come true, partly because of all the post-covid drama that led to the visa restriction for visitors from China mainland, and partly because of my easy-shaken determination deep inside me. Maybe I wasn't longing for that anyway. And I think it doesn't matter anymore, just let it go with the wind.
> Oh, how about things from work? Intense project schedules, difficult tasks, unfamiliar domains, but I can manage to get home on time, mostly. So I have not much to blame, it's what I've been paid for after all. But my contract is going to end at the end of July, which is bothering me somehow. If I have to find another job for a routine life and a stable income, the spare time that allowed me to prepare for RUN will be pretty much limited, but if I give up on finding a new job, the fear of running out of resources will overwhelm me so hard that I can not even breath normally. So my best hope is that the current company will renew another 4-year contract with me, to buy me just a little more time to get my things done. But based on my performance, I'd say that's not something I can grant for. Nobody can.
> Of course, I know there's a huge gap since the last dairy entry has been written, and I'm not going to explain it, let my action explain.

ChatGPT revison

> I just returned from a 5-day break, though I'm not sure whether to call it a vacation or a holiday. Either way, I had originally planned to visit some foreign countries and visit some shooting ranges, but due to post-COVID visa restrictions and my wavering determination, I decided against it. In hindsight, it doesn't matter anymore, and I'm content to let it go.
>
> As for work, it's been challenging with intense project schedules, difficult tasks, and unfamiliar domains, but I have managed to leave on time for the most part. However, my contract is set to end at the end of July, which has me worried. If I have to find another job for stable income and routine life, my spare time to prepare for RUN will be severely limited. But if I don't find a new job, I'll be overwhelmed with fear about running out of resources. So my best hope is for my current company to renew my contract for another 4 years, but I'm not sure that's something I can count on based on my performance.
>
> I realize there's been a significant gap since my last diary entry, but I won't try to explain it. I'll let my actions speak for themselves.