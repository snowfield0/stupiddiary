04-12-2023

I've seen a lot of clips of the cartoon named *Adventure Time* on youtube and finally decided to binge all the episodes, that's the reason why I purchased a subscription from **Hulu**. It's not until then did I found that Hulu only provides a 720p resulotion on website, and a slightly higher resulotion and HDR support on iOS devices,  and chrome even got a crazy 480p spec, which is totally unbearable! I mean, come on dude, It's 2023 already and you think 720p is good enough? I can get 1080p on Netflix and has no problem with 4k option on youtube, but now I have to watch those cartoons in 720p just because their copyrights are hold exculsively by Hulu? That's insane! By all means, I think it's time to go back to pirate website, **just like the old days**.

What makes it worse is that I couldn't fell asleep until at least 4 a.m. in yesterday evening, I mean, this morning. And went to work in 1 hour late in the busy season. Greate Job.

I fell dizzy all the day even on the road, that's not safe and healthy, I think it would be a good idea that to stop doing anything new to me after dinner, in the consideration that I am such a easy-interested guy.

No big deal other than that today, and it's almost the time to start my cambly session tonight, aaaaaand I'll stop here, a little short though, good enough for the 2nd day of the reboot.



Gammarly version (I do not quite agree with some of the modifcations)

> I've seen a lot of clips of the cartoon named *Adventure Time* on youtube and finally decided to binge all the episodes, that's the reason why I purchased a subscription from **Hulu**. It was not until then did I found that Hulu only provides a 720p resolution on the website, and a slightly higher resolution and HDR support on iOS devices, and Chrome even got a crazy 480p spec, which is unbearable! I mean, come on dude, It's 2023 already and you think 720p is good enough? I can get 1080p on Netflix and has no problem with the 4k option on youtube, but now I have to watch those cartoons in 720p just because their copyrights are held exclusively by Hulu? That's insane! By all means, I think it's time to go back to pirating websites, **just like in the old days**.
>
> What makes it worse is that I couldn't fall asleep until at least 4 a.m. in yesterday evening, I mean, this morning. And went to work 1 hour late during the busy season. Great Job.
>
> I fell dizzy all day even on the road, that's not safe and healthy, I think it would be a good idea to stop doing anything new to me after dinner, in consideration that I am such an easy-interested guy.
>
> No big deal other than that today, and it's almost time to start my cambly session tonight, aaaaaand I'll stop here, a little short though, good enough for the 2nd day of the reboot.

ChatGPT version (good as usual, but some meaning were lost and some were misunderstood. It doesn't affect the overall expression of my emotion, but human will not miss those point while rewriting, well, I hope.)

> I recently became a fan of Adventure Time and decided to binge-watch all the episodes. To do so, I purchased a subscription to Hulu, only to discover that the website only offers 720p resolution. On iOS devices, the resolution is slightly higher and HDR support is available, but Chrome only supports a measly 480p resolution. It's frustrating that in 2023, Hulu thinks that 720p is good enough when other streaming services like Netflix and YouTube offer 1080p and even 4K options. It's especially irritating that I have to settle for 720p because of Hulu's exclusive copyrights.
>
> To make matters worse, I had trouble falling asleep last night and ended up being an hour late to work during a busy season. I felt dizzy all day, which isn't safe or healthy. Going forward, I plan to avoid starting anything new after dinner since I'm easily distracted.
>
> Overall, it wasn't a big deal aside from feeling tired and dizzy. It's almost time for my Cambly session tonight, so I'll stop here. Although this entry is shorter than usual, it's a good start for my second day of rebooting.